from bakeforemployee.db.base import Base, engine
import bakeforemployee.db.models


def init_db():
    Base.metadata.create_all(bind=engine)


if __name__ == '__main__':
    init_db()
