# Relation-Task

Wishing your colleague happy birthday by baking a cake for them. Task implemented for relation.

## Getting started
#### Clone the repo
```
$ git clone https://gitlab.com/mufeedpatel/relation-task.git

$ cd bakeforemployee
```

#### Setup DB
Setup virtual env
```
pip install -r requirements.txt
```

Initializing DB
```
$ python3 bakeforemployee/manage.py syncdb
```

Load some sample data 
```
$ python3 bakeforemployee/manage.py loadsampledata
```

#### Sending Happy Birthday Message to a random Employee for baking cake

For sending email to a random employee for baking cake from templates made
```
$ python3 bakeforemployee/manage.py runemailreminder
```

For sending hangout message to a random employee for baking cake from templates made
```
$ python3 bakeforemployee/manage.py runhangoutreminder
```

For sending sms message to a random employee for baking cake from templates made
```
$ python3 bakeforemployee/manage.py runsmsreminder
```

For sending all the above in one shot.
```
$ python3 bakeforemployee/manage.py runallreminders
```
