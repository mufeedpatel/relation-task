
from bakeforemployee.db.makedb import init_db
from bakeforemployee.db.loader import sampledata, exceldata

from bakeforemployee.services.emails import SendEmail
from bakeforemployee.services.sms import SendSms
from bakeforemployee.services.hangout import SendHangoutMessage

from bakeforemployee.helpers import birthdays


syncdb = init_db
loadsampledata = sampledata
loadexceldata = exceldata


def runreminder(service):
    services = {
        'email': SendEmail,
        'sms': SendSms,
        'hangout': SendHangoutMessage
    }

    birthday_members = birthdays()
    for member in birthday_members:
        services[service](member)()


def runallreminders():
    birthday_members = birthdays()
    for member in birthday_members:
        SendEmail(member)()  # send email
        SendHangoutMessage(member)()  # send hangout message
        SendSms(member)()  # send mobile sms


def runemailreminder():
    return runreminder('email')


def runsmsreminder():
    return runreminder('sms')


def runhangoutreminder():
    return runreminder('hangout')
