import sqlite3
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from bakeforemployee.settings import DATABASE

detect_types = sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
engine = create_engine(
    'sqlite:///{}'.format(DATABASE),
    connect_args={'detect_types': detect_types},
    native_datetime=True
)

Base = declarative_base()

DBSession = sessionmaker(bind=engine)

